package experiments;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Icons
{
    public static void main(String[] args) throws IOException
    {
        // Vytvoříme adresář ikony pokud neexistuje
        Files.createDirectories(Paths.get(System.getProperty("user.dir"), "ikony"));

        // Vytvoříme BufferedInputStream na CSV data
        Path inputPath = Paths.get(System.getProperty("user.dir"), "data/hlasy.csv");
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(inputPath.toString()));
        BufferedReader bufferedInputStream = new BufferedReader(inputStreamReader);

        // Přečeteme CSV hlavičku, tu nepotřebujeme
        bufferedInputStream.readLine();

        // Do pole bytes načteme ukázkové BMP jako šablonu (je to malý binární soubor, stream není nutný)
        Path sampleBmpPath = Paths.get(System.getProperty("user.dir"), "data/sample1.bmp");
        byte[] bytes = Files.readAllBytes(Paths.get(sampleBmpPath.toString()));

        // Čteme jednotlivé řádky dokud existují, pro každou je vytvořen nový BMP soubor
        while (true)
        {
            String line = bufferedInputStream.readLine();
            if (line == null)
            {
                break;
            }
            String okres = null;
            String obec = null;
            String okrsek = null;

            // Přečteme data z CSV řádku
            // a upravíme pole bytes podle nich
            // TODO - zde chybí kód

            // Připravíme cestu k novému BMP suboru a definujeme celý jeho obsah bajty z připraveného pole
            Path outputPath = Paths.get(System.getProperty("user.dir"), "ikony", okres + "_" + obec + "_" + okrsek + ".bmp");
            Files.write(outputPath, bytes);
        }
    }
}
